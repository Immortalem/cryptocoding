import random

class SquareAndMultiply():
    def __init__(self):
        pass
        
    def sqmWithOutput(self, base, exponent, modulus):
        binExponent = bin(exponent)[2:]
        print "Binaerdarstellung des Exponenten: %s = %s" %(binExponent, exponent)
            
        val = base
        
        for c in binExponent[1:]:
            old = val
            val = pow(val, 2, modulus)
            print "%8d * %8d mod %8d = %8d" %(old, old, modulus, val)
            if (c == '1'):
                old = val
                val = (val * base) % modulus
                print "%8d * %8d mod %8d = %8d" % (old, base, modulus, val)
        
        print
        return val
        
        
    def sqm(self, base, exponent, modulus):
        binExponent = bin(exponent)[2:]            
        val = base
        
        for c in binExponent[1:]:
            val = pow(val, 2, modulus)
            if (c == '1'):
                val = (val * base) % modulus
                
        return val

class ElGamalWithOutput():
    def __init__(self):
        self.sqm = SquareAndMultiply().sqmWithOutput
        
    def genPubKey(self, alpha, modulus, privKey = 1):
        if (privKey == 1):
            random.seed()
            privKey = random.randint(2, modulus - 2)
            
        print "Calculating public Key"
        key = self.sqm(alpha, privKey, modulus)
        print "Public Key is : %d" % (key)
        return key
        
    def encrypt(self, alpha, beta, plainText, modulus, i = 1):
        if (i == 1):
            random.seed()
            i = random.randint(2, modulus - 2)
        print "Initial values: alpha = %d; beta = %d; modulus = %d; i = %d; plainText = %d" % (alpha, beta, modulus, i, plainText), "\n"
        print "Calculating ephemeral key"
        keyEphemeral = self.sqm(alpha, i, modulus)
        print "Calculating masking key"
        keyMasking   = self.sqm(beta, i, modulus)
        
        print "Ephemeral key: %d" % (keyEphemeral)
        print "Masking key:   %d" % (keyMasking), "\n"
        
        cipherText = (plainText * keyMasking) % modulus
        print "Ciphertext: %d" % (cipherText), "\n"
        
        return cipherText, keyEphemeral
        
    def decrypt(self, cipherText, keyEphemeral, keyPriv, modulus):
        print "Calculating inverse masking key"
        keyMaskingInv = self.sqm(keyEphemeral, modulus - keyPriv - 1, modulus) # Little Fermat's Theorem
        print "Inverse masking key: %d" % (keyMaskingInv)
        
        plainText = (cipherText * keyMaskingInv) % modulus
        print "Plaintext: %d" % (plainText)
        
        return plainText
        
class ElGamal():
    def __init__(self):
        self.sqm = SquareAndMultiply().sqm
        
    def genPubKey(self, alpha, modulus, privKey = 1):
        if (privKey == 1):
            random.seed()
            privKey = random.randint(2, modulus - 2)

        key = self.sqm(alpha, privKey, modulus)

        return key
        
    def encrypt(self, alpha, beta, plainText, modulus, i = 1):
        if (i == 1):
            random.seed()
            i = random.randint(2, modulus - 2)

        keyEphemeral = self.sqm(alpha, i, modulus)
        keyMasking   = self.sqm(beta, i, modulus)
        
        cipherText = (plainText * keyMasking) % modulus
        
        return cipherText, keyEphemeral
        
    def decrypt(self, cipherText, keyEphemeral, keyPriv, modulus):
        keyMaskingInv = self.sqm(keyEphemeral, modulus - keyPriv - 1, modulus) # Little Fermat's Theorem
        
        plainText = (cipherText * keyMaskingInv) % modulus
        
        return plainText


class EllipticCurve():
    def __init__(self, a, b, modulus):
        self.a = a
        self.b = b
        self.modulus = modulus
    
    def addPoint(self, p1, p2):
        if (p1 != None and p2 != None):
            s = 0
            
            x1 = p1[0]
            y1 = p1[1]
            x2 = p2[0]
            y2 = p2[1]
            
            if (p1 == p2):
                ### point doubleling
                s = (((3 * x1 * x1) + self.a) * pow((2 * y1), (self.modulus - 2), self.modulus )) % self.modulus # Little Fermats theorem is used to calculate the inverse
            elif (x1 != x2):
                s = ((y2 - y1) * pow((x2 - x1), (self.modulus - 2), self.modulus)) % self.modulus
            else:
                return None

            x3 = ((s * s) - x1 - x2) % self.modulus
            y3 = (s * (x1 - x3) - y1) % self.modulus

            return (x3 ,y3)

    def checkValidity(self):
        return (((4 * pow(self.a, 3, self.modulus) + (27 * pow(self.b, 2, self.modulus))) % self.modulus) != 0)

    def calculatePoints(self):
        xValues = []
        yValues = []

        points = []

        for i in xrange(self.modulus):
            xValues.append((i, (pow(i, 3) + (self.a * i) + self.b) % self.modulus))
            yValues.append((i, (pow(i, 2) % self.modulus)))

        for tupleX in xValues:
            for tupleY in yValues:
                if (tupleX[1] == tupleY[1]):
                    points.append((tupleX[0], tupleY[0]))

        return points

    def doubleAdd(self, base, multiplier):
        binMultiplier = bin(multiplier)[2:]            
        val = base
        
        for c in binMultiplier[1:]:
            val = self.addPoint(val, val)
            if (c == '1'):
                val = self.addPoint(val, base)
                
        return val
    
    def doubleAddWithOutput(self, base, multiplier):
        binMultiplier = bin(multiplier)[2:]
        print "Binaerdarstellung des Multiplikators: %s = %s" %(binMultiplier, multiplier)
            
        val = base
        
        for c in binMultiplier[1:]:
            old = val
            val = self.addPoint(val, val)
            print "(%4d, %4d) + (%4d, %4d) mod %4d = (%4d, %4d)" % (old[0], old[1], old[0], old[1], self.modulus, val[0], val[1])
            if (c == '1'):
                old = val
                val = self.addPoint(val, base)
                print "(%4d, %4d) + (%4d, %4d) mod %4d = (%4d, %4d)" % (old[0], old[1], base[0], base[1], self.modulus, val[0], val[1])
        
        print
        return val
    
if (__name__ == '__main__'):
    myCurve = EllipticCurve(9, 9, 23)
    
    # print list of all Points on the curve
    print myCurve.calculatePoints()
    
    point = (7,1)
    addedPoint = point
    print '1.', point
    
    for i in xrange(2, 30):
        addedPoint = myCurve.addPoint(addedPoint, point)
        print str(i) + '.', addedPoint
